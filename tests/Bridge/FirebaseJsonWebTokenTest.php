<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt\Tests\Bridge;

use Hermes\Jwt\Bridge\FirebaseJsonWebToken;
use PHPUnit\Framework\TestCase;

/**
 * Class FirebaseJsonWebTokenTest.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FirebaseJsonWebTokenTest extends TestCase
{
    public function testWholeFlow(): void
    {
        $jwt = new FirebaseJsonWebToken('secret');

        $payload = [
            'user-id' => 'someid',
            'sess-id' => 'someId',
        ];

        $token = $jwt->encode($payload);
        $token2 = $jwt->encode($payload);

        $this->assertSame($token, $token2);

        $decoded = $jwt->decode($token);

        $this->assertSame($payload, $decoded);
    }
}
