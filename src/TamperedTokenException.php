<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt;

use Throwable;

/**
 * Class TamperedTokenException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class TamperedTokenException extends JsonWebTokenException
{
    /**
     * @var string
     */
    private $token;

    /**
     * TamperedTokenException constructor.
     *
     * @param string         $token
     * @param Throwable|null $previous
     */
    public function __construct(string $token, ?Throwable $previous = null)
    {
        parent::__construct('The token has been tampered', self::CODE_TAMPERED, $previous);
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
