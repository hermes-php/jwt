<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt;

use Throwable;

/**
 * Class ExpiredTokenException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ExpiredTokenException extends JsonWebTokenException
{
    /**
     * @var string
     */
    private $token;

    /**
     * ExpiredTokenException constructor.
     *
     * @param string         $token
     * @param Throwable|null $previous
     */
    public function __construct(string $token, ?Throwable $previous = null)
    {
        parent::__construct('The token has expired', self::CODE_EXPIRED, $previous);
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
