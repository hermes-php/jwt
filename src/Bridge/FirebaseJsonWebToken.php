<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt\Bridge;

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Hermes\Jwt\ExpiredTokenException;
use Hermes\Jwt\JsonWebToken;
use Hermes\Jwt\JsonWebTokenException;
use Hermes\Jwt\TamperedTokenException;
use Hermes\Jwt\TokenEncodingException;

/**
 * Class FirebaseJsonWebToken.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FirebaseJsonWebToken implements JsonWebToken
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $algorithm;
    /**
     * @var array
     */
    private $supported;
    /**
     * @var string
     */
    private $pubKey;

    /**
     * FirebaseJsonWebToken constructor.
     *
     * @param string      $key       The key use for encoding. If no pubKey is provided, then this is used for decoding too.
     * @param string      $algorithm The algorithm to use for encoding
     * @param array       $supported The list of supported algorithms for decoding
     * @param string|null $pubKey    In case the algorithm is asymmetric. Used only for decoding.
     */
    public function __construct(string $key, string $algorithm = 'HS256', array $supported = ['HS256'], string $pubKey = null)
    {
        $this->key = $key;
        $this->algorithm = $algorithm;
        $this->supported = $supported;
        $this->pubKey = $pubKey;
    }

    /**
     * {@inheritdoc}
     */
    public function encode(array $payload): string
    {
        try {
            return JWT::encode($payload, $this->key, $this->algorithm);
        } catch (\Exception $exception) {
            throw new TokenEncodingException($payload, $exception);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function decode(string $token): array
    {
        try {
            return (array) JWT::decode($token, $this->pubKey ?? $this->key, $this->supported);
        } catch (ExpiredException $exception) {
            throw new ExpiredTokenException($token, $exception);
        } catch (SignatureInvalidException $exception) {
            throw new TamperedTokenException($token, $exception);
        } catch (\UnexpectedValueException $exception) {
            throw new JsonWebTokenException($exception->getMessage(), JsonWebTokenException::CODE_GENERAL, $exception);
        }
    }
}
