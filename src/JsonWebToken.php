<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt;

/**
 * Interface JsonWebToken.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface JsonWebToken
{
    /**
     * Encodes an array into a Json Web Token.
     *
     * The algorithm used and the secret are implementation details.
     *
     * You must configure your adapter to work the way you want.
     *
     * @param array $payload
     *
     * @throws TokenEncodingException when an error occurred encoding the token
     *
     * @return string
     */
    public function encode(array $payload): string;

    /**
     * Decodes a token string into an array.
     *
     * The algorithms supported are an implementation detail.
     *
     * @param string $token
     *
     * @throws JsonWebTokenException  when an error with the token occurs
     * @throws TamperedTokenException when the token has been modified
     * @throws ExpiredTokenException  when the token has expired
     *
     * @return array
     */
    public function decode(string $token): array;
}
