<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt;

use Throwable;

/**
 * Class JsonWebTokenException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class JsonWebTokenException extends \RuntimeException
{
    public const CODE_GENERAL = 1000;
    public const CODE_EXPIRED = 2000;
    public const CODE_TAMPERED = 3000;
    public const CODE_ENCODING_ERROR = 4000;

    /**
     * JsonWebTokenException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
