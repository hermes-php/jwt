<?php

/*
 * This file is part of the Hermes ecosystem.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Jwt;

use Throwable;

/**
 * Class TokenEncodingException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class TokenEncodingException extends JsonWebTokenException
{
    /**
     * @var array
     */
    private $payload;

    /**
     * TokenEncodingException constructor.
     *
     * @param array          $payload
     * @param Throwable|null $previous
     */
    public function __construct(array $payload, ?Throwable $previous = null)
    {
        parent::__construct('There was an error encoding the payload', self::CODE_ENCODING_ERROR, $previous);
        $this->payload = $payload;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }
}
