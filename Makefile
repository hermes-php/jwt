coverage:
	vendor/bin/phpunit --coverage-text

cs:
	vendor/bin/php-cs-fixer fix --diff --verbose

changelog:
	nano CHANGELOG.md

commit: cs coverage changelog
	git add . && git commit

amend: cs coverage changelog
	git add . && git commit --amend